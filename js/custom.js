function stepHandler(counter){
    this.counter = counter;

    this.next = function(){
        this.counter++;
        this.renderView();
    }
    this.back = function(){
        this.counter--;
        this.renderView();
    }
    this.renderView = function() {
        this.manageNextBtn();
        this.manageMenu();
        for(let container of document.getElementsByClassName('main-container')){
            container.style.display = 'none';
        }
        document.getElementsByClassName('screen'+this.counter)[0].style.display = 'block';
        if(this.counter < 2)
            document.getElementsByClassName('main-menu')[0].style.display = 'none';
        else
            document.getElementsByClassName('main-menu')[0].style.display = 'block';

    }
    this.manageNextBtn = function() {
        if(this.counter == 7)
            document.getElementsByClassName('custom-next')[0].style.display = 'none';
        else
            document.getElementsByClassName('custom-next')[0].style.display = 'flex';
    }
  this.manageMenu = function() {
        if(this.counter==1)
            return;
        let menuCount=2;
        for(let menu of document.getElementsByClassName('nav-item')){
            menu.classList.remove('active');
            if(menuCount < this.counter)
                menu.classList.add('visited');
            else
                menu.classList.remove('visited');

            menuCount++;
        }
        document.getElementsByClassName('menu'+this.counter)[0].classList.add('active');
    }
  }
  const step = new stepHandler(1);
  function nextstep() {
    step.next();
  }
  function backstep(){
    step.back();
  }
  step.renderView();

  $("a").click(function(e) {
    e.preventDefault();
  });